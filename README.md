# Sobre o projeto

Este é um projeto em NodeJS desenvolvido utilizando o framework express utilizando o padrão MVC.

##### Documentação da API: http://localhost:3000/api/doc (Disponivel apenas quando subir a aplicação)

## O Desafio

#### Requisitos

- Criar uma API Rest para realizar as seguintes ações:
    - Gerenciar clientes (CRUD) e listar clientes com filtros de status
<br>

## A solução

#### Como rodar o projeto

**Antes de seguir os passos abaixo tenha certeza que o docker e docker-compose estão instalados na maquina. Para rodar este projeto:**

1\. Clone este repositorio  e entre na pasta

```
git clone https://gitlab.com/marcelonascimento.contato/teste_node_f104.git

cd teste_node_f104
```

2\. O arquivo de configurações é o \.env (Já está configurado)\.

3\. Faça o build dos containers \, o container **app** vai usar a porta 3000 e o container **mongo** irá usar a porta 27017, certifique-se que essas portas estejam livres antes de continuar. Se seu usuário não estiver incluido no grupo de permissões do **docker e docker-compose**  será necessário executar os comandos como administrador (sudo)

```
docker-compose up -d
```

4\. Agora você pode acessar aplicação em [localhost ou clique aqui!](http://localhost:3000), para ver a [documentação da API acesse](http://localhost:3000/api/docs)
