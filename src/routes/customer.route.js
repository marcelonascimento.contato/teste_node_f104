const express = require('express');
const router = express.Router();
const customerController = require('./../controllers/customer.controller');
const validate = require('./../middleware/validate');
const customerValidation = require('./../validations/customer.validation');

router
    .route('/')
    .get(validate(customerValidation.getCustomers), customerController.getCustomers)
    .post(validate(customerValidation.createCustomer), customerController.createCustomer)

router
    .route('/:customerId')
    .get(validate(customerValidation.getCustomer), customerController.getCustomer)
    .put(validate(customerValidation.updateCustomer), customerController.updateCustomer)
    .delete(validate(customerValidation.deleteCustomer), customerController.deleteCustomer)

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Customers
 *   description: Customer management and retrieval
 */

/**
 * @swagger
 * /customer:
 *   post:
 *     summary: Create a customer
 *     tags: [Customers]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - name
 *               - document
 *               - phone
 *               - contractNumber
 *               - contractDate
 *               - contractValue
 *               - contractStatus
 *               - action
 *             properties:
 *               name:
 *                 type: string
 *               document:
 *                 type: string
 *               phone:
 *                 type: number
 *               contractNumber:
 *                 type: number
 *               contractDate:
 *                 type: date
 *               contractValue:
 *                 type: number
 *               contractStatus:
 *                 type: string
 *                 enum: ['paid', 'on_time', 'overdue']
 *               action:
 *                 type: string
 *                 enum: ['thank_payment', 'cancel_contract', 'bill']
 *             example:
 *               name: cliente1
 *               document: 47166817852
 *               phone: 1234567
 *               contractNumber: 12996630912
 *               contractValue: 123456
 *               contractStatus: overdue
 *               contractDate: 04/03/1999
 *               action: thank_payment
 *     responses:
 *       "201":
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Customer'
 *
 *   get:
 *     summary: Get all customers
 *     tags: [Customers]
 *     parameters:
 *       - in: query
 *         name: contractStatus
 *         schema:
 *           type: string
 *         description: contract status
 *       - in: query
 *         name: sortBy
 *         schema:
 *           type: string
 *         description: sort by query in the form of field:desc/asc (ex. name:asc)
 *       - in: query
 *         name: limit
 *         schema:
 *           type: integer
 *           minimum: 1
 *         default: 10
 *         description: Maximum number of customers
 *       - in: query
 *         name: page
 *         schema:
 *           type: integer
 *           minimum: 1
 *           default: 1
 *         description: Page number
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 results:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Customer'
 *                 page:
 *                   type: integer
 *                   example: 1
 *                 limit:
 *                   type: integer
 *                   example: 10
 *                 totalPages:
 *                   type: integer
 *                   example: 1
 *                 totalResults:
 *                   type: integer
 *                   example: 1
 */

/**
 * @swagger
 * /customer/{customerId}:
 *   get:
 *     summary: Get a customer
 *     tags: [Customers]
 *     parameters:
 *       - in: path
 *         name: customerId
 *         required: true
 *         schema:
 *           type: string
 *         description: customer id
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Customer'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 *
 *   put:
 *     summary: Update a customer
 *     tags: [Customers]
 *     parameters:
 *       - in: path
 *         name: customerId
 *         required: true
 *         schema:
 *           type: string
 *         description: Customer id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *               document:
 *                 type: string
 *               phone:
 *                 type: number
 *               contractNumber:
 *                 type: number
 *               contractDate:
 *                 type: date
 *               contractValue:
 *                 type: number
 *               contractStatus:
 *                 type: string
 *                 enum: ['paid', 'on_time', 'overdue']
 *               action:
 *                 type: string
 *                 enum: ['thank_payment', 'cancel_contract', 'bill']
 *             example:
 *               name: cliente1
 *               document: 47166817852
 *               phone: 1234567
 *               contractNumber: 12996630912
 *               contractValue: 123456
 *               contractStatus: overdue
 *               contractDate: 04/03/1999
 *               action: thank_payment
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Customer'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 *
 *   delete:
 *     summary: Delete a customer
 *     tags: [Customers]
 *     parameters:
 *       - in: path
 *         name: customerId
 *         required: true
 *         schema:
 *           type: string
 *         description: Customer id
 *     responses:
 *       "200":
 *         description: No content
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 */