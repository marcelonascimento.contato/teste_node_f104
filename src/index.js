const app = require('./app');
const config = require('./config/config');
const mongoose = require('mongoose');

let server;

mongoose.connect('mongodb://mongo:27017/myapp').then(() => {
    server = app.listen(3000, () => {
        console.log('Application ready on port ' + 3000);
    });
});
