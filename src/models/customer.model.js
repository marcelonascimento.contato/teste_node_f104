const mongoose = require('mongoose');
const { customerTypes } = require('./../config/customer');
const { actionTypes } = require('./../config/action');
const { toJSON, paginate } = require('./plugins');

const customerSchema = mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
            trim: true,
            index: true
        },
        document: {
            type: String,
            required: true,
            trim: true,
        },
        phone: {
            type: Number,
            required: true,
            trim: true
        },
        contractNumber: {
            type: Number,
            required: true
        },
        contractDate: {
            type: Date
        },
        contractValue: {
            type: Number,
        },
        contractStatus: {
            type: String,
            required: true,
            enum: [customerTypes.ON_TIME, customerTypes.PAID, customerTypes.OVERDUE],
        },
        action: {
            type: String,
            enum: [actionTypes.THANK_PAYMENT, actionTypes.CANCEL_CONTRACT, actionTypes.BILL],
        }
    }
);

customerSchema.plugin(toJSON);
customerSchema.plugin(paginate);

/**
 * @typedef Customer
 */
 const Customer = mongoose.model('Customer', customerSchema);

module.exports = Customer;