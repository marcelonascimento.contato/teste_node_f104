const express = require('express');
const bodyParser = require('body-parser');
const httpStatus = require('http-status');
const cors = require('cors');
const { errorConverter, errorHandler } = require('./middleware/error');
const ApiError = require('./utils/ApiError');
const routes = require('./routes/index');
const config = require('../src/config/config');

const app = express();

app.set('port', process.env.PORT || 3000);

app.use(bodyParser.urlencoded({ extended: false }))

app.use(cors());

app.options('*', cors());

app.use(bodyParser.json())

app.use('/api', routes)

app.use('/', (req, res) => {
    let docUrl = `http://localhost:${config.port}/api/docs`

    res.send({
        message: `API V1.0, visit documentation in ${docUrl}`
    })
})

app.use((req, res, next) => {
    let docUrl = `http://localhost:${config.port}/api/docs`
    next(new ApiError(httpStatus.NOT_FOUND, `Not found. See documentation in ${docUrl}`));
});

app.use(errorConverter);

app.use(errorHandler);

module.exports = app;