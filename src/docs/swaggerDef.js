const config = require('../config/config');

const swaggerDef = {
  openapi: '3.0.0',
  info: {
    title: 'Express API Documentation',
    version: 1.0,
    description: 'Restful API developed using express + nodeJS',
  },
  contact: {
    name: 'Marcelo Nascimento Dev',
    url: 'https://www.linkedin.com/in/marcelontavares/',
  },
  servers: [
    {
      url: `http://localhost:${config.port}/api`,
      description: 'Development server'
    },
  ],
};

module.exports = swaggerDef;
