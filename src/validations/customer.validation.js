const Joi = require('joi');
const { objectId } = require('./custom.validation');
const { customerTypes } = require('./../config/customer');
const { actionTypes } = require('./../config/action');

const createCustomer = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    document: Joi.number().integer().required(),
    phone: Joi.number().integer().required(),
    contractNumber: Joi.number().integer().required(),
    contractValue: Joi.number().required(),
    contractDate: Joi.date().raw().required(),
    contractStatus: Joi.string().required().valid(customerTypes.OVERDUE, customerTypes.PAID, customerTypes.ON_TIME),
    action: Joi.string().valid(actionTypes.BILL, actionTypes.CANCEL_CONTRACT, actionTypes.THANK_PAYMENT),
  }),
};

const getCustomers = {
  query: Joi.object().keys({
    contractStatus: Joi.valid(customerTypes.OVERDUE, customerTypes.PAID, customerTypes.ON_TIME, '').allow(null),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getCustomer = {
  params: Joi.object().keys({
    customerId: Joi.string().custom(objectId),
  }),
};

const updateCustomer = {
  params: Joi.object().keys({
    customerId: Joi.required().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      name: Joi.string().required(),
      document: Joi.number().integer().required(),
      phone: Joi.number().integer().required(),
      contractNumber: Joi.number().integer().required(),
      contractValue: Joi.number().required(),
      contractDate: Joi.date().raw().required(),
      contractStatus: Joi.string().required().valid(customerTypes.OVERDUE, customerTypes.PAID, customerTypes.ON_TIME),
      action: Joi.string().valid(actionTypes.BILL, actionTypes.CANCEL_CONTRACT, actionTypes.THANK_PAYMENT),    })
    .min(1),
};

const deleteCustomer = {
  params: Joi.object().keys({
    customerId: Joi.string().custom(objectId),
  }),
};

module.exports = {
  getCustomers,
  getCustomer,
  updateCustomer,
  createCustomer,
  deleteCustomer
};