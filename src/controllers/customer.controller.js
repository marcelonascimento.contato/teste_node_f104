const httpStatus = require('http-status');
const pick = require('./../utils/pick');
const ApiError = require('./../utils/ApiError');
const catchAsync = require('./../utils/catchAsync');
const customerService = require('./../services/customer.service');


const createCustomer = catchAsync(async (req, res) => {
   const customer = await customerService.createCustomer(req.body);

   res.status(httpStatus.CREATED).send(customer);
});

const getCustomers = catchAsync(async (req, res) => {
    const filter = pick(req.query, ['contractStatus']);
  
    const options = pick(req.query, ['sortBy', 'limit', 'page']);

    const result = await customerService.queryCustomers(filter, options);

    res.status(httpStatus.OK).send(result);
});

const getCustomer = catchAsync(async (req, res) => {
  const customer = await customerService.getCustomerById(req.params.customerId);
  if (!customer) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Customer not found');
  }

  res.status(httpStatus.OK).send(customer);
});

const updateCustomer = catchAsync(async (req, res) => {
  const customer = await customerService.updateCustomerById(req.params.customerId, req.body);

  res.status(httpStatus.OK).send(customer);
});


const deleteCustomer = catchAsync(async (req, res) => {
    await customerService.deleteCustomerById(req.params.customerId);

    res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
    getCustomers,
    createCustomer,
    getCustomer,
    updateCustomer,
    deleteCustomer
};