const actionTypes = {
    THANK_PAYMENT: 'thank_payment',
    BILL: 'bill',
    CANCEL_CONTRACT: 'cancel_contract',
  };
  
  module.exports = {
    actionTypes,
  };