const customerTypes = {
    OVERDUE: 'overdue',
    ON_TIME: 'on_time',
    PAID: 'paid',
  };
  
  module.exports = {
    customerTypes,
  };